function TinhTienDien(){
    console.log("Bài 2: Tính tiền điện");
    var Hoten = document.getElementById("txtHoTen_B2").value;
    console.log('Hoten: ', Hoten);
    var Kw = document.getElementById("txtNhapSoKw_B2").value * 1;
    console.log('Số Kw: ', Kw);
    var TongTien = 0;

    var gia50KwDau = 500;
    var gia50KwKe = 650;
    var gia100KwKe = 850;
    var gia150KwKe = 1100;
    var giaConlai = 1300;

    if(Kw <= 50){
        TongTien = Kw * gia50KwDau;
        console.log('TongTien: ', new Intl.NumberFormat('ja-JP', {currency: 'JPY'}).format(TongTien));
    }else if(Kw <= 100){
        TongTien = (gia50KwDau * 50) + (Kw - 50) * gia50KwKe;
        console.log('TongTien: ', new Intl.NumberFormat('ja-JP', {currency: 'JPY'}).format(TongTien));
    }else if(Kw <= 200){
        TongTien = ((gia50KwDau * 50) + (gia50KwKe * 50) + ((Kw - 100) * gia100KwKe));
        console.log('TongTien: ', new Intl.NumberFormat('ja-JP', {currency: 'JPY'}).format(TongTien));
    }else if(Kw <= 350){
        TongTien = (gia50KwDau * 50) + (gia50KwKe * 50) + (gia100KwKe * 100) + ((Kw - 200) * gia150KwKe);
        console.log('TongTien: ', new Intl.NumberFormat('ja-JP', {currency: 'JPY'}).format(TongTien));
    }else{
        TongTien = (gia50KwDau * 50) + (gia50KwKe * 50) + (gia100KwKe * 100) + (gia150KwKe * 150) + ((Kw - 350) * giaConlai);
        console.log('TongTien: ', new Intl.NumberFormat('ja-JP', {currency: 'JPY'}).format(TongTien));
    }

    document.getElementById("KQ_2").innerHTML = 
    `<h2>Họ tên: ${Hoten} ; Tiền điện: ${new Intl.NumberFormat('ja-JP', {currency: 'JPY'}).format(TongTien)} </h2>`

    console.log("Họ tên:" + Hoten +"\t;\t"+"Tiền điện:" + new Intl.NumberFormat('ja-JP', {currency: 'JPY'}).format(TongTien));

    // number = 123456;
    // console.log(new Intl.NumberFormat('ja-JP', {currency: 'JPY'}).format(number));

    console.log("---------------------------------------");
}